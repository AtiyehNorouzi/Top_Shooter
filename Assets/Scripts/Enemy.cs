﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Enemy : MonoBehaviour
{
    public enum enemyType { enemy1 = 0, enemy2, enemy3 };

    [SerializeField]
    private Sprite [] enemySprites;

    [SerializeField]
    private Image healthbar_slider;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    public int type;
    public float []health;
    public float []speed;
    public float []damage;

    public void Init(Vector3 dir, enemyType enemytype)
    {
        type = (int)enemytype;
        SetRotation(dir);
        SetEnemyUI();
        StartCoroutine(Move(dir, speed[type]));
    }
    IEnumerator Move(Vector3 dir, float speed)
    {
        while (true)
        {
            transform.Translate(TimeManager.deltaTime * speed * dir, Space.World);
            yield return null;
        }

    }
    public void Damage()
    {
        healthbar_slider.fillAmount -= 1 / health[type];
        if (healthbar_slider.fillAmount <= 0.05f)
            EnemyPool.instance.PushEnemy(this);
    }
    void SetEnemyUI()
    {
        spriteRenderer.sprite = enemySprites[type];
        healthbar_slider.fillAmount = 1;
    }
    void SetRotation(Vector3 dir)
    {
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }
    private void OnBecameInvisible()
    {
        if(gameObject.activeInHierarchy)
            EnemyPool.instance.PushEnemy(this);
    }
}
