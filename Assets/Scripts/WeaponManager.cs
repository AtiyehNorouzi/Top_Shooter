﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public  class WeaponManager : MonoBehaviour
{
    [SerializeField]
    private InputController inputController;

    [System.Serializable]
    public struct WeaponStruct
    {
        public float bulletSpeed;
        public float bulletCapacity;
        public float currentCapacity;
        public float timeToShoot;
        public float inffectRaduis;
        public float timer;
        public bool canShoot;
        public bool reload;
        public bool OnReload;
        public Text currentCapacity_txt;
        public Image timeToShoot_slider;
        public Image reload_slider;
    }
    [SerializeField]
    private Sprite[] weaponSprites;

    public WeaponStruct[] weapons;

    [SerializeField]
    private Transform bulletStartPosition;

    [SerializeField]
    SpriteRenderer spriteRenderer;

    [SerializeField]
    private GameObject enemyParent;

    [SerializeField]
    private GameObject circle;

    int currentWeapon = 0;
    bool canSlide = true;
    const float coolDown = 0.025f;
    const float warmUp =  0.05f;
    void Start()
    {
        inputController.onClicked += ChangeWeapon;
        
        for (int i = 0; i < weapons.Length; i++)
        {
            weapons[i].currentCapacity = weapons[i].bulletCapacity;
            StartCoroutine(WeaponTimeCounter(i));
            if(weapons[i].reload)
                StartCoroutine(CoolDownWeapon(i));
        }
    }
    public void InitBullet(Vector3 target)
    {
        if (weapons[currentWeapon].currentCapacity > 0 && weapons[currentWeapon].canShoot && !weapons[currentWeapon].OnReload)
        {
            Bullet bullet = BulletPool.instance.GetBullet((BulletPool.bulletType)currentWeapon);
            bullet.transform.position = bulletStartPosition.position;
            bullet.transform.rotation = bulletStartPosition.rotation;
            bullet.Init(dir: Vector3.up, speed: weapons[currentWeapon].bulletSpeed);
            weapons[currentWeapon].currentCapacity_txt.text = (--weapons[currentWeapon].currentCapacity).ToString();
            SoundManager.instance.PlaySound(currentWeapon);
            ResetWeaponTimer();
            WarmUpWeapon();
        }
    }
    void ChangeWeapon(InputController.InputType type)
    {
        if(type == InputController.InputType.SwitchWeapon)
        {
            if (currentWeapon == weapons.Length - 1)
                currentWeapon = 0;
            else
                currentWeapon++;
            spriteRenderer.sprite = weaponSprites[currentWeapon];
        }
    }
    void WarmUpWeapon()
    {
        if (weapons[currentWeapon].reload)
        {
            weapons[currentWeapon].reload_slider.fillAmount += warmUp;
            if (Utility.AlmostEqual(weapons[currentWeapon].reload_slider.fillAmount , 1 , 0.05f))
                weapons[currentWeapon].OnReload = true;
        }
    }
    IEnumerator CoolDownWeapon(int index)
    {
        float timer = 0;
        while(true)
        {
            if (weapons[index].OnReload)
            {
                timer += TimeManager.deltaTime * coolDown;
                weapons[index].reload_slider.fillAmount = Mathf.Lerp(1, 0, timer);
                if (Utility.AlmostEqual(weapons[index].reload_slider.fillAmount, 0 , 0.05f))
                {
                    weapons[index].reload_slider.fillAmount = 0;
                    weapons[index].OnReload = false;
                    timer = 0;
                }
            }
            yield return null;
        }
    }
    IEnumerator WeaponTimeCounter(int index)
    {
        while (canSlide)
        {
            if (!weapons[index].canShoot)
            {
                weapons[index].timer += TimeManager.deltaTime * 1f / weapons[index].timeToShoot;
                weapons[index].timeToShoot_slider.fillAmount = Mathf.Lerp(0, 1, weapons[index].timer);
                if (Utility.AlmostEqual(weapons[index].timer , 1 , 0.05f))
                    weapons[index].canShoot = true;
            }
            yield return null;
        }
    }
    void ResetWeaponTimer()
    {
        weapons[currentWeapon].canShoot = false;
        weapons[currentWeapon].timer = 0;
        weapons[currentWeapon].timeToShoot_slider.fillAmount = 0;
    }
    public void AddCapacity(int type , int quantity)
    {
        if (weapons[type].currentCapacity + quantity <= weapons[type].bulletCapacity)
            weapons[type].currentCapacity += quantity;
        else
            weapons[type].currentCapacity = weapons[type].bulletCapacity;
        weapons[type].currentCapacity_txt.text = weapons[type].currentCapacity.ToString();
    }
    public void StartThrowBomb(Vector3 throwPoint)
    {
        StartCoroutine(ThrowBomb(throwPoint));
    }
    IEnumerator ThrowBomb(Vector3 throwPoint)
    {
        circle.transform.position = throwPoint;
        circle.SetActive(true);
        for (int i = 0; i < enemyParent.transform.childCount; i++)
        {
            if (PointInsideCircle(enemyParent.transform.GetChild(i).gameObject.transform.position, throwPoint, weapons[2].inffectRaduis))
            {
                enemyParent.transform.GetChild(i).GetComponent<Enemy>().Damage();
            }
        }
        yield return new WaitForSeconds(1.5f);
        circle.SetActive(false);
    }
    bool PointInsideCircle(Vector3 point , Vector3 center, float radius)
    {
        return Vector3.Distance(point, center) < radius;
    }
}
