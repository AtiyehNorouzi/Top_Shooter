﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    [SerializeField]
    private Sprite [] ammoSprites;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private TextMesh ammoAmount_txt;

    [SerializeField]
    private WeaponManager weaponManager;

    [SerializeField]
    private Animator animator;
   
    [SerializeField]
    GameObject boxParent;

    bool collectAmmo = false;
    int type;
 
    public void Init()
    {
        animator.enabled = false;
        boxParent.SetActive(true);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Bullet" && !collectAmmo)
        {
            StartCoroutine(GetAmmo());
        }
    }
    IEnumerator GetAmmo()
    {
        collectAmmo = true;
        animator.enabled = true;
        animator.Play("CrateDestr", 1, 0);
        type = Random.Range(0, ammoSprites.Length);
        spriteRenderer.sprite = ammoSprites[type];
        int count = Random.Range(Mathf.RoundToInt(weaponManager.weapons[type].bulletCapacity / 6), Mathf.RoundToInt(weaponManager.weapons[type].bulletCapacity / 4));
        ammoAmount_txt.text = count.ToString();
        weaponManager.AddCapacity(type, count);
        yield return new WaitForSeconds(0.7f);
        collectAmmo = false;
        boxParent.SetActive(false);
    }
  
}
