﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour
{
    Vector3 mousePosition;
    Vector3 castPoint;
	void Update ()
    {
        mousePosition = Input.mousePosition;
        castPoint = Camera.main.ScreenToWorldPoint(mousePosition);
        //Set position to mouse position + z offset of camera
        transform.position = castPoint +  Vector3.forward*10;
	}
}
