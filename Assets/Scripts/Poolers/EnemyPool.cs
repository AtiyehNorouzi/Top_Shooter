﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour {

    [System.Serializable]
    public struct enemyObjStruct {
        public int amount;
        public GameObject prefab;
        public Stack<Enemy> pooledObjs;

    }
    public enemyObjStruct enemiesWeWantToPool;
    private GameObject enemyParent;
    private static EnemyPool _instance;
    public static EnemyPool instance {
        get {
            if (_instance == null)
                _instance = FindObjectOfType<EnemyPool> ();

            return _instance;
        }
    }
    void Awake () {
        _instance = this;
        CreateEnemies ();
    }

    public void CreateEnemies () {
        enemiesWeWantToPool.pooledObjs = new Stack<Enemy> ();
        enemyParent = new GameObject (enemiesWeWantToPool.prefab.name + "_parent");
        enemyParent.transform.SetParent (instance.gameObject.transform);

        for (int j = 0; j < enemiesWeWantToPool.amount; j++) {

            GameObject GO = Instantiate (enemiesWeWantToPool.prefab);
            Enemy c = GO.GetComponent<Enemy> ();
            GO.SetActive (false);
            GO.transform.SetParent (enemyParent.transform);
            enemiesWeWantToPool.pooledObjs.Push (c);
        }

    }

    public void PushEnemy (Enemy bullet) {
        enemiesWeWantToPool.pooledObjs.Push (bullet);
        bullet.gameObject.transform.SetParent (enemyParent.transform);
        bullet.gameObject.transform.localPosition = Vector3.zero;
        bullet.gameObject.SetActive (false);
    }

    public Enemy GetEnemy () {
        Enemy p = enemiesWeWantToPool.pooledObjs.Pop ();
        p.gameObject.SetActive (true);
        p.gameObject.transform.SetParent (null);
        return p;
    }
}