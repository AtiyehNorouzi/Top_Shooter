﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{
    public static bool AlmostEqual(float a , float b , float dist)
    {
        if (Mathf.Abs(a - b) <= dist)
            return true;
        return false;
    }
}
