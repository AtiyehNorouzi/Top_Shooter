﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameState : MonoBehaviour
{
    [SerializeField]
    private InputController inputController;

    [SerializeField]
    private GameObject gameOverPanel;

    [SerializeField]
    private GameObject Menu;

    private static GameState _instance;
    public static GameState instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameState>();

            return _instance;
        }
    }
    void Start()
    {
        _instance = this;
        inputController.onClicked += InputListener;
    }
    void InputListener(InputController.InputType type)
    {
        if (type == InputController.InputType.Restart && gameOverPanel.activeSelf)
            Restart();
    }
    public void Restart()
    {
        TimeManager.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void Pause()
    {
        TimeManager.timeScale = 0;
        gameOverPanel.SetActive(true);
    }
    public void StartGame()
    {
        Menu.SetActive(false);
    }
}
