﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;

public class JsonReader : MonoBehaviour
{
    private static JsonReader _instance;
    public static JsonReader instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<JsonReader>();

            return _instance;
        }
    }
    JsonData waveRef;
    string path;
    public string json;
    [System.Serializable]
    public class Wave
    {
        public List<Enemy> enemy;
    }
    [System.Serializable]
    public class Enemy
    {
        public int type;
        public int count;
    }
    [System.Serializable]
    public class GameData
    {
        public List<Wave> waves;
    }
    public class JsonData
    {
        public int ok;
        public GameData data;
    }
    private void Awake()
    {
        path = Application.dataPath + "/StreamingAsset/Waves.json";
        if (File.Exists(path))
            json = File.ReadAllText(path);
    }
    public JsonData GetWaveRef()
    {
        waveRef = JsonConvert.DeserializeObject<JsonData>(json);
        return waveRef;
    }
}
