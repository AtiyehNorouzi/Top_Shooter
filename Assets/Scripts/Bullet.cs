﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private WeaponManager weaponManager;

    public BulletPool.bulletType type;

    private void Start()
    {
        weaponManager = GameObject.FindObjectOfType<WeaponManager>();
    }
    public void Init(Vector3 dir , float speed)
    {
        StartCoroutine(Move(dir, speed));
    }
    IEnumerator Move(Vector3 dir , float speed)
    {
        while(true)
        {
            transform.Translate(TimeManager.deltaTime * speed * dir);
            yield return null;
        }
   
    }
    private void OnBecameInvisible()
    {
        if(gameObject.activeInHierarchy)
            BulletPool.instance.pushBullet(type , this);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy" || collision.tag == "box") 
        {
            if (type == BulletPool.bulletType.Bullet3)
                weaponManager.StartThrowBomb(collision.gameObject.transform.position);
            else
            {
                if(collision.tag == "Enemy")
                     collision.gameObject.GetComponent<Enemy>().Damage();
            }
            BulletPool.instance.pushBullet(type , this);
        }
    }
}
